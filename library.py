import os
import sys
from utils import isdir, isfile, hasextension

class Entry:
    def __init__(self, path:str):
        self.path = path
    
    @property
    def name(self):
        return self.path.rpartition('/')[2]

    def __str__(self):
        return self.name

class Song(Entry):
    @property
    def name(self):
        r = self.path.rpartition('/')[2]
        r = r.rpartition('.')[0]
        r = r.partition('.')[2]
        return r

    @property
    def art(self):
        return self.album.art

    @property
    def artist(self):
        return self.album.artist

    @property
    def album(self):
        return Album(self.path.rpartition('/')[0])

    @property
    def length(self):
        return 300

class Album(Entry):
    @property
    def art(self):
        for entry in os.listdir(self.path):
            if hasextension(entry, ('jpg', 'png')):
                return self.path+'/'+entry

    @property
    def artist(self):
        return Album(self.path.rpartition('/')[0])

    @property
    def songs(self):
        return [Song(self.path+'/'+entry) for entry in os.listdir(self.path) if entry[0] != '.' and hasextension(self.path+'/'+entry, ('mp3', 'wav', 'ogg', 'aac', 'opus'))]

class Artist(Entry):
    @property
    def art(self):
        for entry in os.listdir(self.path):
            if hasextension(entry, ('jpg', 'png')):
                return self.path+'/'+entry

    @property
    def albums(self):
        return [Album(self.path+'/'+entry) for entry in os.listdir(self.path) if entry[0] != '.' and isdir(self.path+'/'+entry)]

    @property
    def songs(self):
        return [song for album in self.albums for song in album.songs]

class Library:
    def __init__(self, path:str):
        self.path = path

    @property
    def artists(self):
        return [Artist(self.path+'/'+entry) for entry in os.listdir(self.path) if entry[0] != '.' and isdir(self.path+'/'+entry)]

    @property
    def albums(self):
        return [album for artist in self.artists for album in artist.albums]
        
    @property
    def songs(self):
        return [song for album in self.albums for song in album.songs]
        
    def print_all(self):
        for artist in self.artists:
            print('artist : ' + artist.name + ' -> ' + artist.path)
            for album in artist.albums:
                print('\talbum : ' + album.name + ' -> ' + album.path)
                for song in album.songs:
                    print('\t\tsong : ' + song.name + ' -> ' + song.path)

if __name__ == '__main__':
    library = Library(sys.argv[1])
    library.print_all()
    print(sys.getsizeof(library.songs))    
else:
    library = Library('/sd')
