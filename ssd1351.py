import time

'''
SSD1351 OLED module
'''

class Display:
    '''
    Serial interface for 16-bit color (5-6-5 RGB) SSD1351 OLED display
    '''
    width = const(128)
    height = const(128)
    SET_COLUMN = const(0x15)
    SET_ROW = const(0x75)
    WRITE_RAM = const(0x5C)
    READ_RAM = const(0x5D)
    SET_REMAP = const(0xA0)
    START_LINE = const(0xA1)
    DISPLAY_OFFSET = const(0xA2)
    DISPLAY_ALL_OFF = const(0xA4)
    DISPLAY_ALL_ON = const(0xA5)
    NORMAL_DISPLAY = const(0xA6)
    INVERT_DISPLAY = const(0xA7)
    FUNCTION_SELECT = const(0xAB)
    DISPLAY_OFF = const(0xAE)
    DISPLAY_ON = const(0xAF)
    PRECHARGE = const(0xB1)
    DISPLAY_ENHANCEMENT = const(0xB2)
    CLOCK_DIV = const(0xB3)
    SET_VSL = const(0xB4)
    SET_GPIO = const(0xB5)
    PRECHARGE2 = const(0xB6)
    SET_GRAY = const(0xB8)
    USE_LUT = const(0xB9)
    PRECHARGE_LEVEL = const(0xBB)
    VCOMH = const(0xBE)
    CONTRAST_ABC = const(0xC1)
    CONTRAST_MASTER = const(0xC7)
    MUX_RATIO = const(0xCA)
    COMMAND_LOCK = const(0xFD)
    HORIZ_SCROLL = const(0x96)
    STOP_SCROLL = const(0x9E)
    START_SCROLL = const(0x9F)

    def __init__(self, spi, cs, dc, rst):
        '''
        Initialize OLED

        Args:
            spi (Class Spi):  SPI interface for OLED
            cs (Class Pin):  Chip select pin
            dc (Class Pin):  Data/Command pin
            rst (Class Pin):  Reset pin
        '''
        self.spi = spi
        self.cs = cs
        self.dc = dc
        self.rst = rst
        self.cs.init(self.cs.OUT, value=1)
        self.dc.init(self.dc.OUT, value=0)
        self.rst.init(self.rst.OUT, value=1)
        self.reset()
        self.write_cmd(self.COMMAND_LOCK, 0x12)  # Unlock IC MCU interface
        self.write_cmd(self.COMMAND_LOCK, 0xB1)  # A2,B1,B3,BB,BE,C1
        self.write_cmd(self.DISPLAY_OFF)  # Display off
        self.write_cmd(self.DISPLAY_ENHANCEMENT, 0xA4, 0x00, 0x00)
        self.write_cmd(self.CLOCK_DIV, 0xF0)  # Clock divider F1 or F0
        self.write_cmd(self.MUX_RATIO, 0x7F)  # Mux ratio
        self.write_cmd(self.SET_REMAP, 0x74)  # Segment remapping
        self.write_cmd(self.START_LINE, 0x00)  # Set Display start line
        self.write_cmd(self.DISPLAY_OFFSET, 0x00)  # Set display offset
        self.write_cmd(self.SET_GPIO, 0x00)  # Set GPIO
        self.write_cmd(self.FUNCTION_SELECT, 0x01)  # Function select
        self.write_cmd(self.PRECHARGE, 0x32),  # Precharge
        self.write_cmd(self.PRECHARGE_LEVEL, 0x1F)  # Precharge level
        self.write_cmd(self.VCOMH, 0x05)  # Set VcomH voltage
        self.write_cmd(self.NORMAL_DISPLAY)  # Normal Display
        self.write_cmd(self.CONTRAST_MASTER, 0x0C)  # Contrast master
        self.write_cmd(self.CONTRAST_ABC, 0xFF, 0xFF, 0xFF)  # Contrast RGB
        self.write_cmd(self.SET_VSL, 0xA0, 0xB5, 0x55)  # Set segment low volt.
        self.write_cmd(self.PRECHARGE2, 0x01)  # Precharge2
        self.write_cmd(self.DISPLAY_ON)  # Display on
        self.clear()

    def write_cmd(self, command, *args):
        '''
        Write command to OLED

        Args:
            command (byte): SSD1351 command code
            *args (optional bytes): Data to transmit
        '''
        self.dc(0)
        self.cs(0)
        self.spi.write(bytearray([command]))
        self.cs(1)
        # Handle any passed data
        if len(args) > 0:
            self.write_data(bytearray(args))

    def write_data(self, data):
        '''
        Write data to OLED

        Args:
            data (bytes): Data to transmit
        '''
        self.dc(1)
        self.cs(0)
        self.spi.write(data)
        self.cs(1)

    def reset(self):
        '''
        Perform reset: Low=initialization, High=normal operation
        '''
        self.rst(0)
        time.sleep(.05)
        self.rst(1)
        time.sleep(.05)

    def on(self):
        '''
        Turn display on
        '''
        self.write_cmd(self.DISPLAY_ON)

    def off(self):
        '''
        Turn display off
        '''
        self.write_cmd(self.DISPLAY_OFF)

    def contrast(self, level):
        '''
        Set display contrast to specified level

        Args:
            level (int): Contrast level (0 - 15)
        '''
        assert(0 <= level < 16)
        self.write_cmd(self.CONTRAST_MASTER, level)

    def draw(self, data):
        '''
        Write a block of data to display.

        Args:
            data (bytes): Data buffer to write
        '''
        self.write_cmd(self.SET_COLUMN, 0, self.width-1)
        self.write_cmd(self.SET_ROW, 0, self.height-1)
        self.write_cmd(self.WRITE_RAM)
        self.write_data(data)

    def clear(self):
        '''
        Clear display
        '''
        self.draw(bytearray(self.width * self.height * 2))

    def cleanup(self):
        '''
        Clean up resources
        '''
        self.clear()
        self.off()
        self.spi.deinit()

# if __name__ == '__main__':
#     img = Image('/sd/RBB_128.jpg')
#     clock = time.clock()
#     for i in range(200):
#         clock.tick()
#         display.draw(img.copy().gamma_corr(contrast=i/100))
#         print(clock.fps())
