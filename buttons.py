from machine import Timer
from Maix import GPIO

class Button:
    def __init__(self, gpio):
        if type(gpio) == int: gpio = GPIO(gpio, GPIO.IN, GPIO.PULL_UP)
        self.gpio = gpio
        self.current = False
        self.previous = False

    def update(self):
        self.previous = self.current
        self.current = self.gpio() == 0

    @property
    def pressed(self):
        return self.current

    @property
    def released(self):
        return not self.current

    @property
    def justpressed(self):
        return self.current and not self.previous

    @property
    def justreleased(self):
        return not self.current and self.previous

class TurboButton(Button):
    def __init__(self, gpio, cooldown=0.08):
        super().__init__(gpio)
        self.cooldown = cooldown
        self.accumulator = 0

    def update(self, delta):
        super().update()
        if self.justpressed:
            self.accumulator = 0
        elif self.pressed:
            self.accumulator += delta
            if self.accumulator >= self.cooldown:
                self.previous = False
                self.accumulator = 0

class Rotary:
    DIR_NONE = 0x00
    DIR_CW = 0x10
    DIR_CCW = 0x20

    R_START = 0x0
    R_CCW_BEGIN = 0x1
    R_CW_BEGIN = 0x2
    R_START_M = 0x3
    R_CW_BEGIN_M = 0x4
    R_CCW_BEGIN_M = 0x5

    transition_table = [
        [R_START_M, R_CW_BEGIN, R_CCW_BEGIN, R_START],
        [R_START_M|DIR_CCW, R_START, R_CCW_BEGIN, R_START],
        [R_START_M|DIR_CW, R_CW_BEGIN, R_START, R_START],
        [R_START_M, R_CCW_BEGIN_M, R_CW_BEGIN_M, R_START],
        [R_START_M, R_START_M, R_CW_BEGIN_M, R_START|DIR_CW],
        [R_START_M, R_CCW_BEGIN_M, R_START_M, R_START|DIR_CCW],
    ]

    def __init__(self, gpioa, gpiob):
        if type(gpioa) == int:
            gpioa = GPIO(gpioa, GPIO.IN, GPIO.PULL_UP)
        if type(gpiob) == int:
            gpiob = GPIO(gpiob, GPIO.IN, GPIO.PULL_UP)

        self.gpioa = gpioa
        self.gpiob = gpiob
        self.state = self.R_START
        self.position = 0

        self.timer = Timer(
            Timer.TIMER0, 
            Timer.CHANNEL0, 
            mode=Timer.MODE_PERIODIC, 
            period=1, 
            unit=Timer.UNIT_MS, 
            callback=self.timer_callback
        )

    def timer_callback(self, arg=None):
        pinstate = (self.gpiob() << 1) | (self.gpioa())
        self.state = self.transition_table[self.state & 0xf][pinstate]
        if self.state & self.DIR_CW:
            self.position += 1
        if self.state & self.DIR_CCW:
            self.position -= 1

    @property
    def scroll(self):
        r = self.position
        self.position = 0
        return r

class FakeRotary:
    def __init__(self, gpio_cw, gpio_ccw):
        self.button_cw = TurboButton(gpio_cw)
        self.button_ccw = TurboButton(gpio_ccw)

    def update(self, delta):
        self.button_cw.update(delta)
        self.button_ccw.update(delta)

    @property
    def scroll(self):
        return self.button_cw.justpressed - self.button_ccw.justpressed
