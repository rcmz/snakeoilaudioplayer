from font import Font
from canvas import Canvas

class View:
    small_font = Font('/sd/.fonts/droid15.uft')
    big_font = Font('/sd/.fonts/droid23.uft')
    background = None

    def draw(self, canvas:Canvas, delta):
        pass
    
    def on_scroll(self, scroll:int):
        pass

    def on_select(self):
        pass

    def on_back(self):
        pass

    def on_playing(self):
        pass

    def on_option(self):
        pass

    def set_background(self, path):
        if path == None:
            self.background = None
        elif self.background == None or self.background.path != path:
            self.background = Canvas(path).luminance(0.5)

    def draw_background(self, canvas:Canvas):
        if self.background != None:
            canvas.add(self.background)
